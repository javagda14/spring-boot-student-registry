package com.javagda14.registry.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.javagda14.registry.model.dto.CreateAppUserDto;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Getter
@Setter
@Entity
@NoArgsConstructor
@AllArgsConstructor
public class AppUser {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String username;

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String password;

    private String name;
    private String surname;

    // lazy - nie ładuj danych dopóki nie są potrzebne
    // cascade - persist sprawi że dane w liście zostaną zapisane do bazy przy zapisie usera
    @OneToMany(fetch = FetchType.LAZY)
    private List<Grade> gradeList;

    // metoda factory która konsumuje CreateAppUserDto i zwraca docelowego usera
    public static AppUser createFromDto(CreateAppUserDto appUserDto) {
        AppUser appUser = new AppUser();
        appUser.setUsername(appUserDto.getUsername());
        appUser.setPassword(appUserDto.getPassword());

        return appUser;
    }
}