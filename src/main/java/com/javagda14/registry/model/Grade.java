package com.javagda14.registry.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.time.LocalDateTime;

@Entity
@Setter
@Getter
@AllArgsConstructor
public class Grade {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private double grade;
    private Subject subject;
    private LocalDateTime dateAdded;

    public Grade() {
        dateAdded = LocalDateTime.now();
    }

    public Grade(Long id, double grade, Subject subject) {
        this.id = id;
        this.grade = grade;
        this.subject = subject;
        this.dateAdded = LocalDateTime.now();
    }
}
