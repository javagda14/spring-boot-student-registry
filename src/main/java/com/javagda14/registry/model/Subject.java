package com.javagda14.registry.model;

public enum Subject {
    LANG_PL,
    LANG_EN,
    MATH,
    IT
}
