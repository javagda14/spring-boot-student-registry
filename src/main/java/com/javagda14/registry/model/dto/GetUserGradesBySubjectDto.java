package com.javagda14.registry.model.dto;

import com.javagda14.registry.model.Subject;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class GetUserGradesBySubjectDto {
    @NotNull
    private Long userId;
    @NotNull
    private Subject subject;
}
