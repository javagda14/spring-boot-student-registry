package com.javagda14.registry.model.dto;

import com.javagda14.registry.model.Subject;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@NoArgsConstructor
public class AddGradeToStudentDto {

    @NotNull
    private AddGradeDto grade;

    @NotNull
    private Long userId;
    /*
    {
        "userId": 1,
        "grade" : {
            "grade": 4.0,
            "subject": "MATH"
        }
    }
     */
}