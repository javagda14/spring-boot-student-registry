package com.javagda14.registry.model.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.javagda14.registry.model.Subject;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@NoArgsConstructor
public class AddGradeDto {
//    @Min(1)
//    @Max(6)

    //    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    // to oznacza że nie wyświetli nam się ta wartość podczas zwracania JSON

    //    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    // to oznacza że nie nie będzie opcji podania tego parametru w json w dto
    // ale będzie widoczny jeśli pobierzemy obiekt i metoda w response entity
    // zwróci nam instancję tego typu

    @Range(min = 1, max = 6)
    private double grade;

    @NotNull
    private Subject subject;
}
