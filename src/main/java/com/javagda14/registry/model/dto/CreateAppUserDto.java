package com.javagda14.registry.model.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class CreateAppUserDto {
    private String username;
    private String password;

    private String passwordConfirm;
}
