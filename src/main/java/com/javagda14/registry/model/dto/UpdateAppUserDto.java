package com.javagda14.registry.model.dto;

import lombok.*;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
@NoArgsConstructor
public class UpdateAppUserDto {
    @NotNull
    private Long updatedUserId;

    private String name;
    private String surname;

    @Size(min = 4, max = 255)
    private String password;
}
