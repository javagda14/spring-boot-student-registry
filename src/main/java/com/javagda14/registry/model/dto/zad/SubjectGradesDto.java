package com.javagda14.registry.model.dto.zad;

import com.javagda14.registry.model.Subject;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class SubjectGradesDto {
    private Subject subject;
    private List<Double> grades;
    private Double mean;
}
/*
        {
            "subject": "MATH",
            "grades": [1.0],
            "mean": 1.0
        }
 */