package com.javagda14.registry.model.dto.zad;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class UserStatsDto {
    private String name;
    private String surname;
    private List<SubjectGradesDto> grades;
    private Double mean;
}
