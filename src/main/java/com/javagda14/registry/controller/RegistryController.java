package com.javagda14.registry.controller;

import com.javagda14.registry.model.AppUser;
import com.javagda14.registry.model.Grade;
import com.javagda14.registry.model.Subject;
import com.javagda14.registry.model.dto.AddGradeToStudentDto;
import com.javagda14.registry.model.dto.GetUserGradesBySubjectDto;
import com.javagda14.registry.model.dto.zad.UserStatsDto;
import com.javagda14.registry.service.RegistryService;
import jdk.nashorn.internal.objects.annotations.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/registry/")
public class RegistryController {

    @Autowired
    private RegistryService registryService;

    @PostMapping("/addgrade/")
    public ResponseEntity addGradeToStudent(@RequestBody AddGradeToStudentDto dto) {
        Optional<AppUser> optionalAppUser = registryService.addGrade(dto);
        if (optionalAppUser.isPresent()) {
            return ResponseEntity.ok(optionalAppUser.get());
        }
        return ResponseEntity.badRequest().build();
    }

    @GetMapping("/addgrade/{id}/{grade}/{subject}")
    public ResponseEntity addGradeToStudent(@PathVariable(name = "id") Long userId,
                                            @PathVariable(name = "grade") Double grade,
                                            @PathVariable(name = "subject") Subject subject) {
        Optional<AppUser> optionalAppUser = registryService.addGrade(userId, grade, subject);
        if (optionalAppUser.isPresent()) {
            return ResponseEntity.ok(optionalAppUser.get());
        }
        return ResponseEntity.badRequest().build();
    }

    @GetMapping("/addgrade")
    public ResponseEntity addGradeToStudentParam(@RequestParam(name = "id") Long userId,
                                                 @RequestParam(name = "grade") Double grade,
                                                 @RequestParam(name = "subject") Subject subject) {
        Optional<AppUser> optionalAppUser = registryService.addGrade(userId, grade, subject);
        if (optionalAppUser.isPresent()) {
            return ResponseEntity.ok(optionalAppUser.get());
        }
        return ResponseEntity.badRequest().build();
    }

    @PostMapping("/getUserGrades")
    public ResponseEntity getGradesBySubject(@Valid @RequestBody GetUserGradesBySubjectDto dto) {
        List<Grade> gradeList = registryService.getGradesBySubject(dto);
        return ResponseEntity.ok(gradeList);
    }

    // żeby zrobić podpunkt B należy dodać drugi parametr którym jest data.
    // następnie przekazać go do serwisu jako drugie kryterium filtrowania.
    @GetMapping("/getWeakStudents/{mean}")
    public ResponseEntity getGradesBySubject(@PathVariable(name = "mean") double mean) {
        List<AppUser> users = registryService.getWeakStudents(mean);
        return ResponseEntity.ok(users);
    }

    @GetMapping("/getUserStats/{id}")
    public ResponseEntity getStats(@PathVariable(name = "id") Long userId) {
        Optional<UserStatsDto> userStatsDto = registryService.getStats(userId);
        if (userStatsDto.isPresent()) {
            return ResponseEntity.ok(userStatsDto.get());
        }
        return ResponseEntity.badRequest().build();
    }

}
