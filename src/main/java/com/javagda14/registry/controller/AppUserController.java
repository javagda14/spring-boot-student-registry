package com.javagda14.registry.controller;

import com.javagda14.registry.model.AppUser;
import com.javagda14.registry.model.dto.CreateAppUserDto;
import com.javagda14.registry.model.dto.NameAndSurnameFilterDto;
import com.javagda14.registry.model.dto.RemoveUserByNameDto;
import com.javagda14.registry.model.dto.UpdateAppUserDto;
import com.javagda14.registry.service.AppUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
// request mapping oznacza że wszystkie metody z tej klasy są dostępne pod adresem /user/
@RequestMapping(path = "/user/")
public class AppUserController {

    @Autowired
    private AppUserService appUserService;

    // request mapping oznacza że adresem uri jest 'add'
    // mappingi składają się na /user/add
//    @RequestMapping(path = "/add", method = RequestMethod.POST)
    @PostMapping("/add")
    public ResponseEntity addUser(@RequestBody CreateAppUserDto user) {
        Optional<AppUser> appUserOptional = appUserService.addUser(user);
        if (appUserOptional.isPresent()) {
            return ResponseEntity.status(HttpStatus.CREATED).build();
        }
        return ResponseEntity.badRequest().build();
    }

    @GetMapping("/get/{id}")
    public ResponseEntity get(@PathVariable(name = "id", required = true) Long id) {
        Optional<AppUser> appUserOptional = appUserService.getUserById(id);
        if (appUserOptional.isPresent()) {
            return ResponseEntity.status(HttpStatus.OK).body(appUserOptional.get());
        }
        return ResponseEntity.badRequest().build();
    }

    @PostMapping("/updatename/{id}/{name}")
    public ResponseEntity updateName(@PathVariable(name = "id", required = true) Long id,
                                     @PathVariable(name = "name", required = true) String name) {
        Optional<AppUser> appUserOptional = appUserService.updateName(id, name);
        if (appUserOptional.isPresent()) {
            // zwracamy zaktualizowanego użytkownika
            return ResponseEntity.status(HttpStatus.OK).body(appUserOptional.get());
        }
        return ResponseEntity.badRequest().build();
    }

    @GetMapping("/updatelastname/{id}/{lastname}")
    public ResponseEntity updateLastName(@PathVariable(name = "id", required = true) Long id,
                                         @PathVariable(name = "lastname", required = true) String lastname) {
        Optional<AppUser> appUserOptional = appUserService.updateLastName(id, lastname);
        if (appUserOptional.isPresent()) {
            // zwracamy zaktualizowanego użytkownika
            return ResponseEntity.status(HttpStatus.OK).body(appUserOptional.get());
        }
        return ResponseEntity.badRequest().build();
    }

    @GetMapping("/all")
    public ResponseEntity getAll() {
        List<AppUser> userList = appUserService.getAllUsers();
        return ResponseEntity.status(HttpStatus.OK).body(userList);
    }

    @GetMapping("/findBySurname/{surname}")
    public ResponseEntity getBySurname(@PathVariable(name = "surname", required = true) String surname) {
        List<AppUser> userList = appUserService.getAllUsersBySurname(surname);
        return ResponseEntity.status(HttpStatus.OK).body(userList);
    }

    // Zadanie domowe, zapytania z DTO
    //1. Stwórz zapytanie z DTO, którego zadaniem jest aktualizować jednocześnie imie, nazwisko
    //2. Stwórz zapytanie z DTO, którego zadaniem jest aktualizować jednocześnie imie, nazwisko, password
    @PostMapping("/update")
    public ResponseEntity update(@Valid @RequestBody UpdateAppUserDto updateNameAndSurnameDto) {
        Optional<AppUser> optionalAppUser = appUserService.update(updateNameAndSurnameDto);

        if (optionalAppUser.isPresent()) {
            return ResponseEntity.ok(optionalAppUser.get());
        }
        return ResponseEntity.badRequest().build();
    }


    //    //3. Stwórz zapytanie do usuwania użytkownika po id
    // żeby użyć tej metody będziemy adresować serwis przez : http://localhost:8080/user/delete/1
    @DeleteMapping("/delete/{id}")
    public ResponseEntity deleteVariable(@PathVariable(name = "id") Long id) {
        boolean success = appUserService.deleteUser(id);
        if (success) {
            return ResponseEntity.ok().build();
        }
        return ResponseEntity.badRequest().build();
    }

    //  Forma z request param
    // żeby użyć tej metody będziemy adresować serwis przez : http://localhost:8080/user/delete?id=1&cos=1
    @DeleteMapping("/delete")
    public ResponseEntity deleteParam(@RequestParam(name = "id") Long id) {
        boolean success = appUserService.deleteUser(id);
        if (success) {
            return ResponseEntity.ok().build();
        }
        return ResponseEntity.badRequest().build();
    }

    //4. Stwórz zapytanie z DTO do usuwania użytkowników z podanym imieniem i nazwiskiem
    @DeleteMapping("/delete/")
    public ResponseEntity deleteRequestBody(@Valid @RequestBody RemoveUserByNameDto dto) {
        int howManyRemoved = appUserService.deleteUser(dto);
        if (howManyRemoved > 0) {
            return ResponseEntity.ok(howManyRemoved);
        }
        return ResponseEntity.badRequest().build();
    }

    //5. Stwórz zapytanie z DTO które filtruje użytkowników po imieniu i nazwisku i zwraca ich listę (nie zwracaj uwagi na duże/małe litery
    @PostMapping("/findByNameAndSurname")
    public ResponseEntity findByNameAndSurname(@Valid @RequestBody NameAndSurnameFilterDto dto) {
        List<AppUser> userList = appUserService.getAllUsersByNameAndSurname(dto);
        return ResponseEntity.status(HttpStatus.OK).body(userList);
    }

    // request mapping oznacza że adresem uri jest 'add'
    // mappingi składają się na /user/add
//    @RequestMapping(path = "/add", method = RequestMethod.POST)
//    @PostMapping("/add")
//    public ResponseEntity bulkAddUsers(@RequestBody CreateAppUserDto user) {
//        Optional<AppUser> appUserOptional = appUserService.addUser(user);
//        if (appUserOptional.isPresent()) {
//            return ResponseEntity.status(HttpStatus.CREATED).build();
//        }
//        return ResponseEntity.badRequest().build();
//    }
}
