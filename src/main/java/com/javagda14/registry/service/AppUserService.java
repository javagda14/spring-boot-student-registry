package com.javagda14.registry.service;

import com.javagda14.registry.model.AppUser;
import com.javagda14.registry.model.dto.CreateAppUserDto;
import com.javagda14.registry.model.dto.NameAndSurnameFilterDto;
import com.javagda14.registry.model.dto.RemoveUserByNameDto;
import com.javagda14.registry.model.dto.UpdateAppUserDto;
import com.javagda14.registry.repository.AppUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class AppUserService {

    @Autowired
    private AppUserRepository appUserRepository;

    public Optional<AppUser> addUser(CreateAppUserDto dto) {
        if (dto.getPassword().equals(dto.getPasswordConfirm())) {
            AppUser createdUser = AppUser.createFromDto(dto);

            createdUser = appUserRepository.save(createdUser);

            return Optional.of(createdUser);
        }

        return Optional.empty();
    }

    public Optional<AppUser> getUserById(Long id) {
        return appUserRepository.findById(id);
    }

    public Optional<AppUser> updateName(Long id, String name) {
        // szukam usera w bazie
        Optional<AppUser> appUserOptional = getUserById(id);

        // po odnalezieniu sprawdzam czy jest w optionalu
        if (appUserOptional.isPresent()) {
            // wydobywam go z optionala
            AppUser user = appUserOptional.get();

            // zmieniam mu name
            user.setName(name);

            // aktualizuje obiekt w bazie
            user = appUserRepository.save(user);

            // zwracam optional z userem
            return Optional.of(user);
        }
        // jeśli nie uda się znaleźć usera z podanym id zwracam pusty optional
        return Optional.empty();
    }

    public Optional<AppUser> updateLastName(Long id, String lastname) {
        // szukam usera w bazie
        Optional<AppUser> appUserOptional = getUserById(id);

        // po odnalezieniu sprawdzam czy jest w optionalu
        if (appUserOptional.isPresent()) {
            // wydobywam go z optionala
            AppUser user = appUserOptional.get();

            // zmieniam mu name
            user.setSurname(lastname);

            // aktualizuje obiekt w bazie
            user = appUserRepository.save(user);

            // zwracam optional z userem
            return Optional.of(user);
        }
        // jeśli nie uda się znaleźć usera z podanym id zwracam pusty optional
        return Optional.empty();
    }

    public List<AppUser> getAllUsers() {
        return appUserRepository.findAll();
    }

    public List<AppUser> getAllUsersBySurname(String surname) {
        return appUserRepository.findAllBySurname(surname);
    }

    public Optional<AppUser> update(UpdateAppUserDto dto) {
        Optional<AppUser> appUserOptional = appUserRepository.findById(dto.getUpdatedUserId());
        if (appUserOptional.isPresent()) {
            AppUser appUser = appUserOptional.get();

            if (dto.getName() != null) {
                appUser.setName(dto.getName());
            }

            if (dto.getSurname() != null) {
                appUser.setSurname(dto.getSurname());
            }

            if (dto.getPassword() != null) {
                appUser.setPassword(dto.getPassword());
            }

            appUser = appUserRepository.save(appUser);

            return Optional.of(appUser);
        }
        return Optional.empty();
    }

    public boolean deleteUser(Long id) {
        if (appUserRepository.existsById(id)) {
            appUserRepository.deleteById(id);
            return true;
        }
        return false;
    }

    // wynik deleteByNameAndSurname zwraca ilość zmodyfikowanych(usuniętych) wierszy
    // ta wartość zwracana jest w górę.
    public int deleteUser(RemoveUserByNameDto dto) {
        return appUserRepository.deleteByNameAndSurname(dto.getName(), dto.getSurname());
    }

    public List<AppUser> getAllUsersByNameAndSurname(NameAndSurnameFilterDto dto) {
        return appUserRepository.findAllByNameContainingIgnoreCaseAndSurnameContainingIgnoreCase(
                dto.getName(),
                dto.getSurname());
    }
}
