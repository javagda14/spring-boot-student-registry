package com.javagda14.registry.service;

import com.javagda14.registry.model.AppUser;
import com.javagda14.registry.model.Grade;
import com.javagda14.registry.model.Subject;
import com.javagda14.registry.model.dto.AddGradeToStudentDto;
import com.javagda14.registry.model.dto.GetUserGradesBySubjectDto;
import com.javagda14.registry.model.dto.zad.SubjectGradesDto;
import com.javagda14.registry.model.dto.zad.UserStatsDto;
import com.javagda14.registry.repository.AppUserRepository;
import com.javagda14.registry.repository.GradeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.thymeleaf.expression.Sets;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@Transactional
public class RegistryService {

    @Autowired
    private AppUserRepository appUserRepository;

    @Autowired
    private GradeRepository gradeRepository;

    public void getAllGradesFromStudent(Long studentId) {
        Optional<AppUser> appUserOpt = appUserRepository.findById(studentId);
        AppUser appUser = appUserOpt.get();

        List<Grade> grades = appUser.getGradeList();
    }

    public Optional<AppUser> addGrade(AddGradeToStudentDto dto) {
        return addGrade(dto.getUserId(), dto.getGrade().getGrade(), dto.getGrade().getSubject());
    }

    public Optional<AppUser> addGrade(Long userId, Double gradeX, Subject subject) {
        Optional<AppUser> optionalAppUser = appUserRepository.findById(userId);
        if (optionalAppUser.isPresent()) {
            AppUser appUser = optionalAppUser.get();

            // tworzymy ocenę
            Grade grade = new Grade();
            grade.setGrade(gradeX);
            grade.setSubject(subject);

            grade = gradeRepository.save(grade);

            // dodajemy ocenę studentowi
            appUser.getGradeList().add(grade);

            // zapisujemy studenta ( z powodu persist nad relacją zapisuje się również ocena)
            appUser = appUserRepository.save(appUser);

            // zwracamy wynik
            return Optional.of(appUser);
        }

        return Optional.empty();
    }

    public List<Grade> getGradesBySubject(GetUserGradesBySubjectDto dto) {
        Optional<AppUser> optionalAppUser = appUserRepository.findById(dto.getUserId());
        if (optionalAppUser.isPresent()) {
            List<Grade> grades = optionalAppUser.get().getGradeList();

            return grades
                    .stream()
                    .filter(grade -> grade.getSubject() == dto.getSubject())
                    .collect(Collectors.toList());
        }
        return new ArrayList<>();
    }

    public List<AppUser> getWeakStudents(double mean) {
        List<AppUser> appUsers = appUserRepository.findAll();

        return appUsers.stream().filter(appUser -> {
            return appUser.getGradeList()
                    .stream()
                    // todo: filtr daty trafia tutaj
                    .map(Grade::getGrade)
                    .mapToDouble(p -> p).average()
                    .getAsDouble() < mean;
        }).collect(Collectors.toList());
    }

    public Optional<UserStatsDto> getStats(Long userId) {
        Optional<AppUser> optionalAppUser = appUserRepository.findById(userId);
        if (optionalAppUser.isPresent()) {
            AppUser user = optionalAppUser.get();

            UserStatsDto userStatsDto = new UserStatsDto();
            userStatsDto.setName(user.getName());
            userStatsDto.setSurname(user.getSurname());

            // obliczenie średniej WSZYSTKICH OCEN!
            userStatsDto.setMean(user.getGradeList().stream()
                    .map(Grade::getGrade)
                    .mapToDouble(p -> p).average().orElse(0.0)); // ORELSE - jeśli nie będzie ocen to zwracamy 0.0

            // pobieram listę wszystkich przedmiotów
            Set<Subject> subjects = user.getGradeList()
                    .stream()
                    .map(Grade::getSubject)
                    .collect(Collectors.toSet());

            List<SubjectGradesDto> subjectGrades = subjects.stream().map(subject -> {
                SubjectGradesDto subjectGradesDto = new SubjectGradesDto();
                subjectGradesDto.setSubject(subject);
                /// do tego miejsca ^^ ustawiam tylko subject i przygotowuje obiekt do zwrócenia


                subjectGradesDto.setGrades(user.getGradeList()
                        // pobieram z użytkownika listę WSZYSTKICH OCEN
                        .stream()
                        .filter(grade -> grade.getSubject() == subject)
                        // FILTRUJE PODANY PRZEDMIOT ^^
                        .map(Grade::getGrade)
                        // Wyciągam tylko oceny ^^
                        .collect(Collectors.toList()));
                        // zbieram wszystkie oceny w listę ^^

                subjectGradesDto.setMean(
                        subjectGradesDto.getGrades()
                                // biorę wszystkie oceny z tego przedmiotu
                                .stream()
                                .mapToDouble(grade -> grade)
                                // mapuję na double
                                .average()
                                // obliczam średnią (zwraca optional double)
                                .orElse(0.0));
                                // zwróć 0.0 jeśli nie znalazłeś oceny
                return subjectGradesDto;
            }).collect(Collectors.toList());

            // ustawiamy grades na stat
            userStatsDto.setGrades(subjectGrades);

            return Optional.of(userStatsDto);
        }
        return Optional.empty();
    }
}